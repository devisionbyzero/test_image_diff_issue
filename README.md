Project to test issue with images diff and gitattributes

Steps:

1.Create empty repo
2.Add images - diff is working
3.Add gitattributes, png - binary
4.Change images - diff is not working (expected behaviour)
5.Add png diff to gitattributes
